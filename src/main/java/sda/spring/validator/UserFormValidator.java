package sda.spring.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import sda.spring.form.UserBean;

@Component
public class UserFormValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return UserBean.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		UserBean user = (UserBean) target;
		if (null == user.getUsername()){
			errors.rejectValue("username", "Username field is mandatory");
		}
		
		if (null == user.getPassword()){
			errors.rejectValue("password", "Password field is mandatory");
		}
	}

}
