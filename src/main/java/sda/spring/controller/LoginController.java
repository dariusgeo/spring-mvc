package sda.spring.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import sda.spring.form.UserBean;
import sda.spring.validator.UserFormValidator;

@Controller
public class LoginController {
	
	@Autowired
	private UserFormValidator userFormValidator;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userFormValidator);
	}

	@RequestMapping(value = "/home",  method = GET)
	public ModelAndView goToLoginPage() {
		Map<String, UserBean> model = new HashMap<String, UserBean>();
		model.put("user", new UserBean());
		
		return new ModelAndView("login", model);
	}
	
	@RequestMapping(value = "/login",  method = POST)
	public ModelAndView authenticateUser(@ModelAttribute("user") UserBean user,  ModelMap model) {
		model.put("user", user);
		
		return new ModelAndView("loginSuccessful", model);
	}
	
}
