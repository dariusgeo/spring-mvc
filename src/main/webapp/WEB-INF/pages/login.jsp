<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:url value="/login" var="login" htmlEscape="true"/>
<div align="center">
	<form:form id="form" modelAttribute="user"  method="post" action="${login}">	
	    <form:label path="username">Username:</form:label>
	    <form:input path="username"/><br><br> 
	    <form:label path="password">Password:</form:label>
	    <form:password path="password"/><br><br>
		<form:button>Login</form:button>
	</form:form>
</div>

